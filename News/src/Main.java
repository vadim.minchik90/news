import java.awt.EventQueue;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JLabel;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import java.awt.GridLayout;
import java.awt.Color;
import javax.swing.JScrollPane;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.DropMode;
import java.awt.Font;

public class Main {
	private JFrame frame;
	private JTextField textField;
	private JTextField textFieldSearchBar;
	private JPanel resultsPane = new JPanel();

	/**
	 * Launch the application.
	 */

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	public Main() throws URISyntaxException, IOException, InterruptedException {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @throws URISyntaxException
	 * @throws InterruptedException
	 * @throws IOException
	 */
	private void initialize() throws URISyntaxException, IOException, InterruptedException {
		Set<Article> news = new HashSet();
		Set<Article> overwritingAFriendsSheet = new HashSet();
		Document docBelnovosti = Jsoup.connect("https://www.belnovosti.by/").get();
		Elements sectionsFlexsRowsBelnovosti = docBelnovosti.getElementsByAttributeValue("class", "ex_box");
		for (Element sectionFlexRow : sectionsFlexsRowsBelnovosti) {
			Element entranceToClassAElementsBelnovosti = sectionFlexRow.child(0);
			Element aElementsBelnovosti = entranceToClassAElementsBelnovosti.child(0);
			Article article = new Article();
			article.setData(sectionFlexRow.getElementsByClass("date").first().ownText());
			article.setUrl("https://www.belnovosti.by/" + aElementsBelnovosti.attr("href"));
			article.setTitle(aElementsBelnovosti.attr("title"));
			news.add(article);
		}

		frame = new JFrame();
		frame.setResizable(false);
		frame.setTitle("Новостник");
		frame.setBounds(100, 100, 1029, 507);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		resultsPane.setBounds(254, 22, 516, 129);
		resultsPane.setLayout(new GridLayout(0, 1, 0, 0));

		JScrollPane mainPanelScrolling = new JScrollPane(resultsPane);
		mainPanelScrolling.setBounds(0, 30, 1015, 439);
		frame.getContentPane().add(mainPanelScrolling);
		
		textFieldSearchBar = new JTextField();
		textFieldSearchBar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textFieldSearchBar.setToolTipText("");
		textFieldSearchBar.setBounds(0, 0, 917, 32);
		frame.getContentPane().add(textFieldSearchBar);
		
		showNews(news);

		JButton buttonToFind = new JButton("Найти");
		buttonToFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				overwritingAFriendsSheet.clear();
				String stringTextFieldSearchBar = textFieldSearchBar.getText().toLowerCase();
				for (Article article : news) {
					if (article.getTitle().toLowerCase().contains(stringTextFieldSearchBar)) {
						overwritingAFriendsSheet.add(article);
					}
				}
				showNews(overwritingAFriendsSheet);
				frame.printAll(frame.getGraphics());
				
				
			}
		});
		buttonToFind.setBounds(914, 0, 99, 30);
		frame.getContentPane().add(buttonToFind);

	}

	private void showNews(Set<Article> overwritingAFriendsSheet) {
		resultsPane.removeAll();
		for (Article article : overwritingAFriendsSheet) {
			JPanel cyclicPanel = new JPanel();
			FlowLayout flowLayout = (FlowLayout) cyclicPanel.getLayout();
			flowLayout.setAlignment(FlowLayout.LEFT);

			JLabel JLabelNewsHeadline = new JLabel(article.getTitle() + "  ");
			JLabelNewsHeadline.setOpaque(true);
			cyclicPanel.add(JLabelNewsHeadline);
			
			JButton buttonGoToArticle = new JButton("Перейти на статью");
			buttonGoToArticle.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						java.awt.Desktop.getDesktop().browse(java.net.URI.create(article.getUrl()));
					} catch (java.io.IOException e1) {
						System.out.println(e1.getMessage());
					}
				}
			});
			cyclicPanel.add(buttonGoToArticle);
			
			JLabel lblNewLabel_1 = new JLabel("  Дата выхода новости  " + article.getData());
			cyclicPanel.add(lblNewLabel_1);
			
			resultsPane.add(cyclicPanel);
		}
		
	}
}