import java.awt.EventQueue;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.Color;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import java.awt.Dimension;
import javax.swing.JScrollBar;
import javax.swing.JToggleButton;
import javax.swing.JPasswordField;
import javax.swing.JSlider;
import javax.swing.JSeparator;

public class Main {
	private JFrame frame;

	/**
	 * Launch the application.
	 */

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	public Main() throws URISyntaxException, IOException, InterruptedException {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @throws URISyntaxException
	 * @throws InterruptedException
	 * @throws IOException
	 */
	private void initialize() throws URISyntaxException, IOException, InterruptedException {
		List<Article> news = new ArrayList();

		HttpClient client = HttpClient.newHttpClient();
		HttpRequest requestWeather = HttpRequest.newBuilder(new URI("https://www.tut.by/")).build();
		HttpResponse<String> responseWeather = client.send(requestWeather, BodyHandlers.ofString());
		String htmlWeather = responseWeather.body().toString();
		Pattern myPatternWeatherNow = Pattern.compile("<span class=\"w-weather_temt\">(.\\d+)");
		Pattern myPatternOblochnost = Pattern.compile("<div class=\"w-weather_note\">(.+)<\\/div>");
		Pattern myPatternWeatherInDifferentTimePeriods = Pattern.compile("<dt class=\"w-weather_time\">(.+)<\\/dt>");
		Pattern myPatternWeatherInNumbers = Pattern.compile("<dd class=\"w-weather_temp\">(.+)&deg;<\\/dd>");
		Matcher myMatcherWeatherNow = myPatternWeatherNow.matcher(htmlWeather);
		Matcher myMatcherOblochnost = myPatternOblochnost.matcher(htmlWeather);
		Matcher myMatcherWeatherInDifferentTimePeriods = myPatternWeatherInDifferentTimePeriods.matcher(htmlWeather);
		Matcher myMatcherWeatherInNumbers = myPatternWeatherInNumbers.matcher(htmlWeather);
		myMatcherWeatherNow.find();
		myMatcherOblochnost.find();
		myMatcherWeatherInDifferentTimePeriods.find();
		myMatcherWeatherInNumbers.find();

		Document doc = Jsoup.connect("https://www.belnovosti.by/").get();
		Elements sectionsFlexsRows = doc.getElementsByAttributeValue("class", "f");
		sectionsFlexsRows.forEach(sectionFlexRow -> {
			Element aElements = sectionFlexRow.child(0);
			Article article = new Article();
			article.setURL("https://www.belnovosti.by/" + aElements.attr("href"));
			article.setTitle(aElements.attr("title"));
			news.add(article);
		});

		frame = new JFrame();
		frame.setTitle("Новостник");
		frame.setBounds(100, 100, 732, 480);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel JLabelWeatherNow = new JLabel(myMatcherWeatherNow.group(1));
		JLabelWeatherNow.setBounds(10, 130, 71, 34);
		JLabelWeatherNow.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(JLabelWeatherNow);

		JLabel JLabelOblochnost = new JLabel(myMatcherOblochnost.group(1));
		JLabelOblochnost.setBounds(66, 134, 243, 26);
		frame.getContentPane().add(JLabelOblochnost);

		JLabel JLabelWeatherInNumbers_1 = new JLabel(myMatcherWeatherInNumbers.group(1));
		JLabelWeatherInNumbers_1.setBounds(10, 22, 71, 34);
		JLabelWeatherInNumbers_1.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(JLabelWeatherInNumbers_1);

		JLabel JLabelWeatherInDifferentTimePeriods_1 = new JLabel(myMatcherWeatherInDifferentTimePeriods.group(1));
		JLabelWeatherInDifferentTimePeriods_1.setBounds(66, 26, 110, 26);
		frame.getContentPane().add(JLabelWeatherInDifferentTimePeriods_1);

		myMatcherWeatherInNumbers.find();// второй раз так делаю чтоб достать следущие совпадение
		JLabel JLabelWeatherInNumbers_2 = new JLabel(myMatcherWeatherInNumbers.group(1));
		JLabelWeatherInNumbers_2.setBounds(10, 50, 71, 34);
		JLabelWeatherInNumbers_2.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(JLabelWeatherInNumbers_2);

		myMatcherWeatherInDifferentTimePeriods.find();// второй раз так делаю чтоб достать следущие совпадение
		JLabel JLabelWeatherInDifferentTimePeriods_2 = new JLabel(myMatcherWeatherInDifferentTimePeriods.group(1));
		JLabelWeatherInDifferentTimePeriods_2.setBounds(66, 54, 110, 26);
		frame.getContentPane().add(JLabelWeatherInDifferentTimePeriods_2);

		myMatcherWeatherInNumbers.find();// третий раз так делаю чтоб достать следущие совпадение
		JLabel JLabelWeatherInNumbers_3 = new JLabel(myMatcherWeatherInNumbers.group(1));
		JLabelWeatherInNumbers_3.setBounds(10, 78, 71, 34);
		JLabelWeatherInNumbers_3.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(JLabelWeatherInNumbers_3);

		myMatcherWeatherInDifferentTimePeriods.find();// третий раз так делаю чтоб достать следущие совпадение
		JLabel JLabelWeatherInDifferentTimePeriods_3 = new JLabel(myMatcherWeatherInDifferentTimePeriods.group(1));
		JLabelWeatherInDifferentTimePeriods_3.setBounds(66, 82, 110, 26);
		frame.getContentPane().add(JLabelWeatherInDifferentTimePeriods_3);

		JLabel JLabelWeatherInMinsk = new JLabel("Погода в Минске");
		JLabelWeatherInMinsk.setBounds(10, 0, 124, 24);
		JLabelWeatherInMinsk.setFont(new Font("Tahoma", Font.PLAIN, 15));
		JLabelWeatherInMinsk.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(JLabelWeatherInMinsk);

		JLabel JLabelInRussianWordsTheWeatherIsNow = new JLabel("Погода сейчас");
		JLabelInRussianWordsTheWeatherIsNow.setBounds(20, 119, 103, 14);
		JLabelInRussianWordsTheWeatherIsNow.setFont(new Font("Tahoma", Font.PLAIN, 14));
		JLabelInRussianWordsTheWeatherIsNow.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(JLabelInRussianWordsTheWeatherIsNow);

//		JButton buttonBack = new JButton("<");
//		buttonBack.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				--variableForNews;
//				textField.setText(articleTitleList.get(variableForNews).getTitle());
//			}
//		});
//		buttonBack.setBounds(144, 0, 58, 26);
//		frame.getContentPane().add(buttonBack);

//		JButton buttonNextPage = new JButton(">");
//		buttonNextPage.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				if (variableForNews <= articleTitleList.size()) {
//					++variableForNews;
//				}
//				textField.setText(articleTitleList.get(variableForNews).getTitle());
//			}
//		});
//		buttonNextPage.setBounds(700, 0, 58, 26);
//		frame.getContentPane().add(buttonNextPage);
//
//		JButton buttonTheArticle = new JButton("Перейти на статью");
//		buttonTheArticle.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				try {
//					java.awt.Desktop.getDesktop().browse(java.net.URI.create(articleUrlList.get(variableForNews).getURL()));
//				} catch (java.io.IOException e1) {
//					System.out.println(e1.getMessage());
//				}
//			}
//		});
//		buttonTheArticle.setBounds(200, 0, 502, 26);
//		frame.getContentPane().add(buttonTheArticle);

//		textField = new JTextField();
//		textField.setEditable(false);
//		textField.setText(articleTitleList.get(variableForNews).getTitle());
//		textField.setHorizontalAlignment(SwingConstants.CENTER);
//		textField.setBounds(144, 23, 614, 26);
//		frame.getContentPane().add(textField);
//		textField.setColumns(10);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(254, 22, 516, 129);
		panel_1.setBackground(Color.ORANGE);
		panel_1.setLayout(new GridLayout(0, 1, 0, 0));

		JScrollPane jScrollPane = new JScrollPane(panel_1);
		jScrollPane.setBounds(10, 175, 696, 255);

		for (Article article : news) {
			JLabel lblNewLabel = new JLabel(article.getTitle());
			lblNewLabel.setOpaque(true);
			panel_1.add(lblNewLabel);

			JButton buttonTheArticle = new JButton("Перейти на статью");
			buttonTheArticle.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						java.awt.Desktop.getDesktop().browse(java.net.URI.create(article.getURL()));
					} catch (java.io.IOException e1) {
						System.out.println(e1.getMessage());
					}
				}
			});

			panel_1.add(buttonTheArticle);

		}

		frame.getContentPane().add(jScrollPane);

	}
}